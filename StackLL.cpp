#include "StackLL.h"
#include <iostream>

using namespace std;

class Stack::Node
{
	public:
		int data = 0;
		Node* link = nullptr;
};

Stack::~Stack()
{
	while(num_elements<0)
	pop();
}	

int Stack::size()
{
	return num_elements;
}

void Stack::push(int num)
{
	Node* newPtr = new Node(num);
	newPtr->link = frontPtr;
	frontPtr = newPtr;
	
	num_elements++;
}
   
void Stack::pop()
{
	Node* delptr = frontPtr;
	frontPtr = frontPtr -> link;
	
	delete delptr;
	num_elements--;
}

int Stack::top()
{
	return frontPtr->data;
}

void Stack::clear()
{
	while(num_elements >0)
	pop();
}

